﻿using System;

namespace ImagineStore.BLL
{
    /// <summary>
    /// Contains customer's data
    /// </summary>
    public class Customer
    {
        private readonly string _firstName;
        private readonly string _lastName;
        internal readonly DateTime _birthDay;

        /// <summary>
        /// 1 - Low valuable customer
        /// 2 - Medium valuable customer
        /// 3 - High valuable customer
        /// </summary>
        internal readonly Segment _segment;

        public Customer(string firstName, string lastName, DateTime birthDay, Segment segment)
        {
            _firstName = firstName;
            _lastName = lastName;
            _birthDay = birthDay;
            _segment = segment;
        }
    }
}
