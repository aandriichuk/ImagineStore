﻿namespace ImagineStore.DAL
{
    public class HolidayFactorDetails
    {
        public int FactorKind { get; set; }

        public decimal Factor { get; set; }
    }
}
