﻿namespace ImagineStore.DAL
{
    public class DiscountDetails
    {
        public int SegmentId { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountDollars { get; set; }
    }
}
