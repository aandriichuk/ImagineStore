﻿using ImagineStore.BLL.Interfaces;
using ImagineStore.DAL.Interfaces;
using System;

namespace ImagineStore.BLL
{
    /// <summary>
    /// Implements discount manager
    /// </summary>
    public class DiscountManager : IDiscountManager
    {
        /// <summary>
        /// Provides data access layer to discount storage
        /// </summary>
        private readonly IDiscountRepository _discountRepository;

        public DiscountManager(IDiscountRepository discountRepository)
        {
            _discountRepository = discountRepository;
        }

        /// <summary>
        /// For high-val-cust, mid-val-cust, low-val-cust we provide 10$+0%, 50$+20%, 100$+40% accordingly.
        /// We are doubling discount on customer's birthday.
        /// We are providing 90% discount at most.
        /// </summary>
        /// <param name="amount">Price of order</param>
        /// <param name="customer">Customer's data</param>
        /// <returns>Amount of discount</returns>
        public decimal CalculateDiscount(decimal amount, Customer customer)
        {
            var discountDetails =_discountRepository.GetDiscountDetails((int)customer._segment);
            var result = amount * discountDetails.DiscountPercentage + discountDetails.DiscountDollars;

            if (customer._birthDay == DateTime.Today)
            {
                var holidayFactor = _discountRepository.GetHolidayFactor((int)HolidayFactorKind.BirthDay);
                result *= holidayFactor;
            }

            var totalDiscountPercent = result * 100 / amount;
            if (totalDiscountPercent > 90)
                result = amount * 0.9m;

            return result;
        }
    }
}
