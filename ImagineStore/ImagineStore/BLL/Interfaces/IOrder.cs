﻿using System.Collections.Generic;

namespace ImagineStore.BLL.Interfaces
{
    /// <summary>
    /// Represents an order
    /// </summary>
    public interface IOrder
    {
        /// <summary>
        /// Order items. 
        /// </summary>
        List<OrderItem> Items { get; set; }

        /// <summary>
        /// Calculates discount for specific customer
        /// </summary>
        /// <param name="customer">Customer's data</param>
        /// <returns></returns>
        decimal CalculateDiscountForCustomer(Customer customer);
    }
}
