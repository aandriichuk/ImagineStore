﻿using System;
using NUnit.Framework;
using System.Collections;
using ImagineStore.BLL;
using Moq;
using ImagineStore.DAL.Interfaces;
using ImagineStore.DAL;

namespace ImagineStore.Test
{
    [TestFixture]
    [Category("Discount Unit Tests")]
    [Category("Unit")]
    public class DiscountTests
    {
        private Mock<IDiscountRepository> discountRepository;

        public static IEnumerable TestCases
        {
            get
            {
                yield return new TestCaseData(750m, 750m, "Gill", "Bates", DateTime.Today, Segment.High, 1350m);
            }
        }

        [SetUp]
        public void Init()
        {
            discountRepository = new Mock<IDiscountRepository>();
            discountRepository.Setup(r => r.GetDiscountDetails(It.IsAny<int>())).Returns(new DiscountDetails
            {
                DiscountPercentage = 0.4m,
                DiscountDollars = 100,
                SegmentId = 3
            });
            discountRepository.Setup(r => r.GetHolidayFactor(It.IsAny<int>())).Returns(2m);
        }

        [Test, TestCaseSource("TestCases")]
        public void DiscountTest(decimal price1, decimal price2, string firstName, string lastName, DateTime birthDay, Segment segment, decimal expectedDiscount)
        {
            // ARRANGE
            var discountManager = new DiscountManager(discountRepository.Object);
            var order = new Order(discountManager);
            order.Items.AddRange(new OrderItem[] { new OrderItem(price1), new OrderItem(price2) });
            var customer = new Customer(firstName, lastName, birthDay, segment);

            // ACT
            var actualDiscount = order.CalculateDiscountForCustomer(customer);

            // ASSERT
            Assert.AreEqual(expectedDiscount, actualDiscount);
        }
    }
}
