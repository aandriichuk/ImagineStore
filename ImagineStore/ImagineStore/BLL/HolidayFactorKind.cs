﻿namespace ImagineStore.BLL
{
    public enum HolidayFactorKind
    {
        BirthDay = 1,
        Christmas = 2
    }
}
