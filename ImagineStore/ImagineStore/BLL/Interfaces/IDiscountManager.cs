﻿namespace ImagineStore.BLL.Interfaces
{
    /// <summary>
    /// Represents discount manager
    /// </summary>
    public interface IDiscountManager
    {
        /// <summary>
        /// Calculates discount 
        /// </summary>
        /// <param name="amount">Price of order</param>
        /// <param name="customer">Customer's data</param>
        /// <returns>Amount of discount</returns>
        decimal CalculateDiscount(decimal amount, Customer customer);
    }
}
