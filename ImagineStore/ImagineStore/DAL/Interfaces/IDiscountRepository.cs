﻿namespace ImagineStore.DAL.Interfaces
{
    public interface IDiscountRepository
    {
        DiscountDetails GetDiscountDetails(int segment);

        decimal GetHolidayFactor(int factorKind);
    }
}
