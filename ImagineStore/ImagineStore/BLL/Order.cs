﻿using ImagineStore.BLL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ImagineStore.BLL
{
    /// <summary>
    /// Implements order functionality
    /// </summary>
    public class Order : IOrder
    {
        private readonly IDiscountManager _discountManager;

        public Order(IDiscountManager discountManager)
        {
            _discountManager = discountManager;
            Items = new List<OrderItem>();
        }

        public List<OrderItem> Items { get; set; }

        public decimal CalculateDiscountForCustomer(Customer customer)
        {
            var itemsTotal = Items.Sum(i => i._itemPrice);
            return _discountManager.CalculateDiscount(itemsTotal, customer);
        }
    }
}
