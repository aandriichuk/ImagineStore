/*
 * 
 * Problem:
 * We have a small piece of code which we believe you can make better.
 * The domain of the code is some imagine store which provides discounts for their lovely customers.
 * They want to ask you to refactor this code to simplify the future maintenance
 * Keep in mind that they are crazy with requirements and want to stay pretty flexible on decisions.
 * 
 * Timing: up to you. Avarage time is 60-90 minutes
 * 
 * What's important:
 *   Code maintainability and testability:
 *     - Will it be easy to maintain your design in future? 
 *     - Is it testable in an environment of a real project?
 *   Design flexibility:
 *     - Will it easy to add new discount type? e.g. discount by segment, discount by segment
 *     - Will it easy to add new multiplier type? e.g. multiplier discounts on birthday, on Christmas
 *     - Will it be easy to store discounts and multipliers in a database?
 *     - Is your design follow best practices in software design? e.g. SOLID
 *   Algorithm's correctness:
 *     - Is your changes follow the method's annotation?
 *     
 *   Feel free to add some ToDo for a reviewer of your code.
 *   Feel free to comment your hard decision on your design.
 *   
 */

using System;

namespace InterviewCode1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var customer = new Customer
            {
                FirstName = "Gill",
                LastName = "Bates",
                BirthDay = DateTime.Today,
                Segment = 3
            };

            var orderPrice = 1500;

            var discountPrice = orderPrice - DiscountManager.CalculateDiscount(orderPrice, customer);

            Console.WriteLine(new String('=', 20));
            Console.WriteLine("Order price :   {0}", orderPrice);
            Console.WriteLine("Discount    :   {0}", -orderPrice + discountPrice);
            Console.WriteLine(new String('=', 20));
            Console.WriteLine("Total price :   {0}", discountPrice);
            Console.WriteLine(new String('=', 20));
        }
    }

    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime BirthDay { get; set; }

        /// <summary>
        /// 1 - Low valuable customer
        /// 2 - Medium valuable customer
        /// 3 - High valuable customer
        /// </summary>
        public int Segment { get; set; }
    }

    public static class DiscountManager
    {
        /// <summary>
        /// For high-val-cust, mid-val-cust, low-val-cust we provide 10$+0%, 50$+20%, 100$+40% accordingly.
        /// We are doubling discount on customer's birthday.
        /// We are providing 90% discount at most.
        /// </summary>
        public static decimal CalculateDiscount(decimal amount, Customer customer)
        {
            decimal result = 0;
            if (customer.Segment == 1)
            {
                return result + 10;
            }
            else if (customer.Segment == 2)
            {
                return result * 0.3m + 50;
            }
            else if (customer.Segment == 3)
            {
                return result * 0.6m + 100;
            }
            if (customer.BirthDay == DateTime.Today)
            {
                result *= 2;
            }
            return result;
        }
    }
}
