﻿namespace ImagineStore.BLL
{
    /// <summary>
    /// Contains order item data
    /// </summary>
    public class OrderItem
    {
        /// <summary>
        /// Order item price
        /// </summary>
        internal readonly decimal _itemPrice;

        public OrderItem(decimal itemPrice)
        {
            _itemPrice = itemPrice;
        }
    }
}
