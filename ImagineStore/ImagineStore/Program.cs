﻿using ImagineStore.BLL;
using ImagineStore.BLL.Interfaces;
using ImagineStore.DAL;
using ImagineStore.DAL.Interfaces;
using Ninject;
using Ninject.Extensions.Conventions;
using System;
using System.Linq;

namespace ImagineStore
{
    class Program
    {
        private static IKernel Kernel = new StandardKernel();

        static void Main(string[] args)
        {
            RegisterContainer();

            var order = Kernel.Get<Order>(); 
            order.Items.AddRange(new OrderItem[] { new OrderItem(750), new OrderItem(750) });

            var customer = new Customer(firstName: "Gill", lastName: "Bates", birthDay: DateTime.Today, segment: Segment.High);
            var discount = order.CalculateDiscountForCustomer(customer);

            var orderPrice = order.Items.Sum(i => i._itemPrice);
            var discountPrice = orderPrice - discount;

            Console.WriteLine(new string('=', 20));
            Console.WriteLine($"Order price :   {orderPrice}");
            Console.WriteLine($"Discount    :  -{discount}");
            Console.WriteLine(new string('=', 20));
            Console.WriteLine($"Total price :   {discountPrice}");
            Console.WriteLine(new string('=', 20));

            Console.ReadLine();
        }

        static void RegisterContainer()
        {
            Kernel.Bind<IDiscountManager>().To<DiscountManager>();
            Kernel.Bind<IDiscountRepository>().To<DiscountRepository>();
            Kernel.Bind<IOrder>().To<Order>();
        }
    }
}
