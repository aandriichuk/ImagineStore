﻿using ImagineStore.DAL.Interfaces;
using System.Linq;

namespace ImagineStore.DAL
{
    public class DiscountRepository : IDiscountRepository
    {
        public DiscountDetails GetDiscountDetails(int segment)
        {
            return new DiscountDetails[]
            {
                new DiscountDetails
                {
                    DiscountPercentage = 0,
                    DiscountDollars = 10,
                    SegmentId = 1
                },
                new DiscountDetails
                {
                    DiscountPercentage = 0.2m,
                    DiscountDollars = 50,
                    SegmentId = 2
                },
                new DiscountDetails
                {
                    DiscountPercentage = 0.4m,
                    DiscountDollars = 100,
                    SegmentId = 3
                }
            }
            .Where(d => d.SegmentId == segment).Single();
        }

        public decimal GetHolidayFactor(int factorKind)
        {
            return new HolidayFactorDetails[]
            {
                new HolidayFactorDetails
                {
                    FactorKind = 1,
                    Factor = 2
                },
                new HolidayFactorDetails
                {
                    FactorKind = 2,
                    Factor = 2
                },
            }
            .Where(d => d.FactorKind == factorKind).Single().Factor;
        }
    }
}
